import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navigation from "./components/Navigation/Navigation";
import Messages from "./components/Messages/Messages";
import Contacts from "./components/Contacts/Contacts";
import Preferences from "./components/Preferences/Preferences";
import data from "./data/messages.json";

const App = () => {
  const folderFilter = () => {
    const folder: any = new Set(
      data.map((v: any) => {
        return v.folder;
      })
    );
    return [...folder];
  };

  const getUser = () => {
    const user: any = new Set(
      data.map((v: any) => {
        return v.to;
      })
    );
    return [...user];
  };

  const toMySelf = () => {
    return data.filter((v: any) => {
      return v.to === "devguy@angular.dev";
    });
  };

  return (
    <div className="App">
      <Router>
        <Navigation user={getUser} />
        <div className="container-fluid p-0">
          <Switch>
            <Route path="/messages">
              <Messages folderFilter={folderFilter} toMySelf={toMySelf} />
            </Route>
            <Route path="/contacts">
              <Contacts />
            </Route>
            <Route path="/pref">
              <Preferences />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
};

export default App;
