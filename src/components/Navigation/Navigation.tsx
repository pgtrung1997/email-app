import React, { useState } from "react";
import { Link } from "react-router-dom";
import style from "./Navigation.module.css";

interface INavigationProps {
  user: () => string[];
}

const Navigation = (props: INavigationProps) => {
  const [select, setSelect] = useState<string>("");
  const { user } = props;

  const onSelect = (e: React.MouseEvent<HTMLElement>) => {
    setSelect((e.target as any).textContent);
  };

  return (
    <nav className="navbar navbar-expand w-100 justify-content-between">
      <ul className="navbar-nav" onClick={onSelect}>
        <li className="nav-item nav-header">
          <Link
            className={`nav-link ${
              select === "Messages" ? style["active"] : ""
            }`}
            to="/messages"
          >
            Messages
          </Link>
        </li>
        <li className="nav-item nav-header">
          <Link
            className={`nav-link ${
              select === "Contacts" ? style["active"] : ""
            }`}
            to="/contacts"
          >
            Contacts
          </Link>
        </li>
        <li className="nav-item nav-header">
          <Link
            className={`nav-link ${
              select === "Preferences" ? style["active"] : ""
            }`}
            to="/pref"
          >
            Preferences
          </Link>
        </li>
      </ul>
      <div className="nav-right">
        <select name="user" className="mx-3  border-0">
          {user().map((v: string, i: number) => {
            return (
              <option value={v} key={i}>
                {v}
              </option>
            );
          })}
        </select>
        <button className="btn btn-primary mx-1">Home</button>
        <button className="btn btn-primary mx-1">New Messages</button>
      </div>
    </nav>
  );
};

export default Navigation;
