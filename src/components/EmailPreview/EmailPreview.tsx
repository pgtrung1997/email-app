import React from "react";
import { useParams, useRouteMatch } from "react-router-dom";
import styles from "./EmailPreview.module.css";
import { Link } from "react-router-dom";

interface IEmailPreviewProps {
  toMySelf: () => any;
}

const EmailPreview = (props: IEmailPreviewProps) => {
  const { toMySelf } = props;
  const { id } = useParams<{ id: string }>();
  let { url } = useRouteMatch<{ url: string }>();

  const date = (value: string) => {
    let arr = value.split("T")[0];
    return arr;
  };

  return (
    <div className={`w-100 ${styles.emailPreview}`}>
      <table className="table table-hover text-left">
        <thead>
          <tr>
            <th>Sender</th>
            <th>Subject</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
          {toMySelf().map((v: any, i: number) => {
            if (v.folder === id) {
              return (
                <tr key={i}>
                  <td>
                    <Link
                      className="text-dark text-decoration-none"
                      to={`${url}/${v._id}`}
                    >
                      {v.from}
                    </Link>
                  </td>
                  <td>
                    <Link
                      className="text-dark text-decoration-none"
                      to={`${url}/${v._id}`}
                    >
                      {v.subject}
                    </Link>
                  </td>
                  <td>
                    <Link
                      className="text-dark text-decoration-none"
                      to={`${url}/${v._id}`}
                    >
                      {date(v.date)}
                    </Link>
                  </td>
                </tr>
              );
            }
          })}
        </tbody>
      </table>
    </div>
  );
};

export default EmailPreview;
