import React from "react";
import { useParams } from "react-router-dom";
import style from "./EmailDetail.module.css";

interface IEmailDetailProps {
  toMySelf: () => any;
}

const EmailDetail = (props: IEmailDetailProps) => {
  const { detailId } = useParams<{ detailId: string }>();
  const { toMySelf } = props;

  const convertTime = (value: string) => {
    let arr = value.split("T");

    // Convert month number to month name
    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    let month: string[] = arr[0].split("-");
    month[1] = monthNames[Number(month[1]) - 1];

    // Convert 24 hour time of day to 12 hour time
    let time = arr[1].split("+")[0];
    let H: number = Number(time.substr(0, 2));
    let h = H % 12 || 12;
    let ampm = H < 12 || H === 24 ? "AM" : "PM";
    time = `${h}${time.substr(2)} ${ampm}`;
    return (
      <p>
        {month[1]} {month[2]}, {month[0]} {time}
      </p>
    );
  };

  const breakLine = (value: string) => {
    return value.replace(/\n/g, "\n\n");
  };

  return (
    <>
      {toMySelf().map((v: any, i: number) => {
        if (v._id === detailId) {
          return (
            <>
              <div className="detail" key={i}>
                <div className={style["header-detail"]}>
                  <div className={style["header-left"]}>
                    <h5>{v.subject}</h5>
                    <p className="m-0">
                      {v.from} &rarr; {v.to}
                    </p>
                  </div>
                  <div className={style["header-right"]}>
                    {convertTime(v.date)}
                    <button className="btn btn-primary mr-2">Reply</button>
                    <button className="btn btn-primary mr-2">Forward</button>
                    <button className="btn btn-primary mr-2">Delete</button>
                  </div>
                </div>
                <div className={style["body-detail"]}>
                  <p>{breakLine(v.body)}</p>
                </div>
              </div>
            </>
          );
        }
      })}
    </>
  );
};

export default EmailDetail;
