import React from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import EmailBox from "../EmailBox/EmailBox";
import EmailPreview from "../EmailPreview/EmailPreview";
import EmailDetail from "../EmailDetail/EmailDetail";
import styles from "./Messages.module.css";

interface IMessagesProps {
  folderFilter: () => string[];
  toMySelf: () => any;
}

const Messages = (props: IMessagesProps) => {
  let { path } = useRouteMatch();
  const { folderFilter, toMySelf } = props;

  return (
    <>
      <div className={`d-flex ${styles.myMessages}`}>
        <EmailBox folderFilter={folderFilter} />
        <Switch>
          <Route path={`${path}/:id`}>
            <EmailPreview toMySelf={toMySelf} />
          </Route>
        </Switch>
      </div>
      <Switch>
        <Route path={`${path}/:id/:detailId`}>
          <EmailDetail toMySelf={toMySelf} />
        </Route>
      </Switch>
    </>
  );
};

export default Messages;
