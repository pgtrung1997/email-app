import React, { useState } from "react";
import { Link, useRouteMatch } from "react-router-dom";
import style from "./EmailBox.module.css";

interface IEmailBoxProps {
  folderFilter: () => string[];
}

const EmailBox = (props: IEmailBoxProps) => {
  let { url } = useRouteMatch();
  const [select, setSelect] = useState<string>("");
  const { folderFilter } = props;

  const onSelect = (e: React.MouseEvent<HTMLElement>) => {
    setSelect((e.target as any).textContent);
  };

  return (
    <ul
      className={`nav flex-column justify-content-center bg-light px-0 ${style["email-box"]}`}
      onClick={onSelect}
    >
      {folderFilter().map((v: any, i: number) => {
        return (
          <li className="nav-item" key={i}>
            <Link
              to={`${url}/${v}`}
              className={`nav-link text-left ${style["text-color"]} ${
                select === v ? style["active"] : ""
              }`}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-folder mx-1"
                viewBox="0 0 16 16"
              >
                <path d="M.54 3.87.5 3a2 2 0 0 1 2-2h3.672a2 2 0 0 1 1.414.586l.828.828A2 2 0 0 0 9.828 3h3.982a2 2 0 0 1 1.992 2.181l-.637 7A2 2 0 0 1 13.174 14H2.826a2 2 0 0 1-1.991-1.819l-.637-7a1.99 1.99 0 0 1 .342-1.31zM2.19 4a1 1 0 0 0-.996 1.09l.637 7a1 1 0 0 0 .995.91h10.348a1 1 0 0 0 .995-.91l.637-7A1 1 0 0 0 13.81 4H2.19zm4.69-1.707A1 1 0 0 0 6.172 2H2.5a1 1 0 0 0-1 .981l.006.139C1.72 3.042 1.95 3 2.19 3h5.396l-.707-.707z" />
              </svg>
              {v}
            </Link>
          </li>
        );
      })}
    </ul>
  );
};

export default EmailBox;
